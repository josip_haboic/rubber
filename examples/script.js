(function () {
    var pres = document.getElementsByClassName('code');
    var examples = document.getElementsByClassName('example');
    for (let example of examples) {
        for (let pre of pres) {
            if (example.id.slice(-1) == pre.id.slice(-1)) {
                pre.innerText = example.innerHTML;
            }
        }
    }
})();

(function() {
    var menuIcon = document.getElementById('menu-control');
    menuIcon.addEventListener('click', function() {
        var menuItems = document.getElementsByClassName('navigation-item');
        var state = menuItems[0].style.display != 'none' ? 'none' : 'flex';
        for (let item of menuItems) {
            item.style.display = state;
        }
    });
})();