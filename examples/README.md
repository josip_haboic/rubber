# How to

## Install

```bash
npm install
```

## Serve
```bash
npm run serve
```

On http://0.0.0.0:4000 you should see the examples.